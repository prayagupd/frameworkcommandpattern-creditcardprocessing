package action;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by prayagupd
 * on 4/15/15.
 */

public class AccountManager {
  private Action action;
  public void submitAction(Action actionToExecute){
    action = actionToExecute;
    action.execute();
  }
}
