package creditcard;
import java.util.Date;

public class CreditCardAccount {
	private long ccNumber;
	private Date expDate;
	private double previousBalance;
	private double currentBalance;

	public double getPreviousBalance(){
		return previousBalance;
	}
	
	public double getCurrentBalance(){
		return currentBalance;
	}
	
	public double getTotalCredit(){
		return 0d;
	}
	
	public double getTotalCreditCharges(){
		return 0d;
	}	
	
	public double getDueAmount(){
		return 0d;
	}


}
